import main
import pandas as pd
import matplotlib.pyplot as plt

example_slp_df = main.get(
    start=pd.to_datetime("2019-01-01 00:00:00"),
    end=pd.to_datetime("2020-01-01 00:00:00"),
    slp_type="H0",
    country="DE",
    state="DE-BY",
    annual_energy_consumption=500,
)

print(example_slp_df)
plt.plot(example_slp_df["time"], example_slp_df["power"])
plt.show()
