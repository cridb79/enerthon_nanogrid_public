"""Dashboard powered by Plotly Dash."""

import time
import dash
import base64
from dash import dcc, html
from dash.dependencies import Input, Output, State
from enerthon_nanogrid.PV_Planning_script import pv_requirements, heat_requirement


def create_app():
    """Dash/flask app factory.

    Note that the flask app object corresponds to Dash's app.server!"""

    external_stylesheets = [
        "./assets/style.css",
        "https://codepen.io/chriddyp/pen/bWLwgP.css",
    ]

    app = dash.Dash(
        __name__,
        meta_tags=[{"name": "viewport", "content": "width=device-width"}],
        external_stylesheets=external_stylesheets,
    )
    app.config.suppress_callback_exceptions = True

    app.layout = html.Div(
        children=[
            html.Div(
                [
                    html.Img(src="./assets/OLT_Logo_horizontal_RGB_L.png"),
                    html.Img(src="./assets/SE_cut.png"),
                ],
                id="header",
            ),
            html.H1(
                children="Planning for an optimal PV system setup",
                style={"margin-top": "50px", "margin-left": "25px"},
            ),
            html.Br(),
            dcc.Input(
                id="internal_temp",
                type="number",
                placeholder="internal temperature",
                style={"margin-right": "10px", "margin-left": "25px"},
            ),
            dcc.Input(
                id="ambient_temp",
                type="number",
                placeholder="ambient temperature",
                style={"margin-right": "10px"},
            ),  # debounce=True),
            dcc.Input(
                id="total_area_walls",
                type="number",
                placeholder="total wall area",
                style={"margin-right": "10px"},
            ),
            dcc.Input(
                id="total_area_floors",
                type="number",
                placeholder="total floor area",
                style={"margin-right": "10px"},
            ),
            html.Div(
                id="heat_output",
                children="Enter values for heating requirements.",
                style={"margin-top": "15px", "margin-left": "25px"},
            ),
            html.Br(),
            dcc.Input(
                id="surface_tilt",
                type="number",
                placeholder="surface tilt",
                style={"margin-right": "10px", "margin-left": "25px"},
            ),
            dcc.Input(
                id="surface_azimuth",
                type="number",
                placeholder="aurface azimuth",
                style={"margin-right": "10px"},
            ),  # debounce=True),
            dcc.Input(
                id="cost_per_module",
                type="number",
                placeholder="cost per module",
                style={"margin-right": "10px"},
            ),
            dcc.Input(
                id="annual_load",
                type="number",
                placeholder="annual load",
                style={"margin-right": "10px"},
            ),
            dcc.Input(
                id="roof_area",
                type="number",
                placeholder="roof area in sqm",
                style={"margin-right": "10px"},
            ),
            html.Br(),
            html.Button(
                "Estimate",
                id="calculate-button",
                n_clicks=0,
                style={"margin-top": "15px", "margin-left": "25px"},
            ),
            dcc.Loading(
                id="loading",
                type="default",
                children=html.Div(
                    html.Div(
                        id="solution",
                        children="Enter values and press calculate.",
                        style={
                            "margin-top": "15px",
                            "margin-left": "25px",
                            "padding-bottom": "25px",
                        },
                    )
                ),
            ),
            html.Div(
                [
                    html.H2(
                        "SAGE-BOT- the Smart Automated Green-Energy & Battery-Optimizing Real-Time Controller",
                        style={
                            "text-align": "center",
                            "margin-top": "20px",
                            "padding-top": "20px",
                            "border-top": "1px solid #636363",
                            "color": "white",
                        },
                    ),
                    html.Div(
                        [
                            html.Ul(
                                [
                                    html.Li(
                                        "Takes all dynamic components of decentralized green energy systems:"
                                    ),
                                    html.Ul(
                                        [
                                            html.Li("Photovoltaic Production (PV)"),
                                            html.Li(
                                                "Battery Systems (Storage and E-Cars)"
                                            ),
                                            html.Li("Energy Consumer Load"),
                                            html.Li("Energy Grid (Buying and Selling)"),
                                        ]
                                    ),
                                    html.Li(
                                        "Optimize everything from an energetic and economic point-of-view"
                                    ),
                                ]
                            ),
                            html.Div(
                                html.Video(
                                    src="./assets/SWIVT2_Controller_mitECar_mov_v4_smooth.mp4",
                                    controls=True,
                                    loop=True,
                                    width=960,
                                ),
                                style={"display": "inline-block"},
                            ),
                        ],
                        style={
                            "display": "flex",
                            "flex-wrap": "wrap",
                            "justify-content": "space-around",
                            "margin-left": "25px",
                            "margin-top": "50px",
                            "padding-bottom": "25px",
                            "margin-right": "25px",
                        },
                    ),
                ],
                style={
                    "color": "white",
                    "background-color": "#636363",
                },
            ),
        ]
    )

    @app.callback(
        Output("heat_output", "children"),
        Input("internal_temp", "value"),
        Input("ambient_temp", "value"),
        Input("total_area_walls", "value"),
        Input("total_area_floors", "value"),
    )
    def update_heat_output(int_temp, amb_temp, area_walls, area_floors):
        heating_req = heat_requirement(
            int_temp, amb_temp, area_walls, area_floors, area_floors
        )
        return u"The heating requirement is {}".format(heating_req)

    @app.callback(
        Output("solution", "children"),
        Input("calculate-button", "n_clicks"),
        State("surface_tilt", "value"),
        State("surface_azimuth", "value"),
        State("cost_per_module", "value"),
        State("annual_load", "value"),
        State("roof_area", "value"),
        prevent_initial_call=True,
    )
    def update_pv_output(n_clicks, tilt, azi, cost, ann, roof):
        pv_req = pv_requirements(tilt, azi, cost, ann, roof)
        return u"The optimal setup is{}".format(pv_req)

    return app.server
