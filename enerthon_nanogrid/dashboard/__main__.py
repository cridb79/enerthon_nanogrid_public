"""Console script for the dashboard."""

import click

from . import create_app


@click.command()
@click.option("--port", default=5000, help="port")
def main(port):
    """Run the dashboard in development mode."""
    app = create_app()
    app.run(port=port, debug=True)


if __name__ == "__main__":
    main()
