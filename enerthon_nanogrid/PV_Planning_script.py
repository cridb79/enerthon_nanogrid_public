import zipfile
from lxml import etree
import matplotlib.pyplot as plt
import re
import dateutil.parser
from io import BytesIO
import os
import urllib.request
from urllib.request import urlopen
import numpy as np
import pandas as pd
import pvlib

import numpy as np
from scipy.signal import argrelextrema

# from enerthon_nanogrid.const import CCKEY, DATA_DIR
from enerthon_nanogrid.weather_data import WeatherData
from enerthon_nanogrid.pv_forecast import PVForecast

from slp_generator.standard_load_profile_generator.main import get
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px

module_database_1 = pvlib.pvsystem.retrieve_sam(name="SandiaMod")


# Heating requirements


def heat_requirement(
    internal_temp, ambient_temp, total_area_walls, total_area_floors, total_ceiling_area
):

    temperature_lift = internal_temp - ambient_temp
    print(temperature_lift)

    # heat loss
    # surface_heat_loss = length * breadth * thermal conductivity
    surface_heat_loss_walls = total_area_walls * 0.8  # W m−1 K−1. #thermal conductivtiy
    surface_heat_loss_floors = total_area_floors * 2.955

    surface_heat_loss_ceilings = total_ceiling_area * 0.8
    # U-Value acts as the heat transfer coefficient and basically measures how quickly heat will escape through different materials.
    # addition of wall, floors etc
    total_heat_loss = (
        surface_heat_loss_walls + surface_heat_loss_floors + surface_heat_loss_ceilings
    )
    print(total_heat_loss)

    heating_required = total_heat_loss * temperature_lift

    return heating_required


"""Script to load weather data, derive PV profiles and optimize
   the number of PV modules and battery size if the load needs to be balanced
   by the PV power alone.
"""
module_database_1 = pvlib.pvsystem.retrieve_sam(name="SandiaMod")

# PV system requirments
# parameters

# pv_module_name": "Canadian_Solar_CS5P_220M___2009_",
# inverter_name": "ABB__MICRO_0_25_I_OUTD_US_208__208V_",
# surface_tilt": 0,
# surface_azimuth": 180,  # pvlib uses 0=North, 90=East, 180=South, 270=West convention
# n_modules_max": 40,  # constrained by the available roof area
# ann_consumption": 2000,  # annual energy consumption in kWh
# cost_per_module": 200.0,
# cost_per_kWh": 10.0,
# ini_charge_level": 10.0,


def pv_requirements(
    surface_tilt, surface_azimuth, cost_per_module, annual_load, roof_area
):

    # parameters
    params = {
        "lat": 48.545146,  # latitude close to Passau
        "lon": 13.353054,  # longitude close to Passau
        "pv_module_name": "Canadian_Solar_CS5P_220M___2009_",
        "inverter_name": "ABB__MICRO_0_25_I_OUTD_US_208__208V_",
        "surface_tilt": surface_tilt,
        "surface_azimuth": 180,  # pvlib uses 0=North, 90=East, 180=South, 270=West convention
        "n_modules_max": 40,  # constrained by the available roof area
        "ann_consumption": annual_load,  # annual energy consumption in kWh
        "cost_per_module": cost_per_module,
        "cost_per_kWh": 10.0,
        "ini_charge_level": 10.0,  # in kWh # this could be set to the integral of the load for one night
    }

    area = module_database_1[params["pv_module_name"]].Area
    no_of_modules = roof_area / area

    # get weather data
    wd = WeatherData(params["lat"], params["lon"])

    # get example weather data from openweather files
    # this is for a specific location, independent of the lat/lon given above
    # contains some missing data!
    df_weather = wd.get_openweather_data(
        filename="OpenWeather_Actuals.csv",
        ilocation=0,
        columns=["temperature", "pressure", "wind_speed", "clouds_all"],
        start="2019-07-01",
        end="2020-07-01",
    )

    # calculate model for PV power forecast
    pvf = PVForecast(params["lat"], params["lon"])

    # get SLP load profile of a given size in [W]
    example_slp_df = get(
        start=pd.to_datetime("2019-07-01 00:00:00"),
        end=pd.to_datetime("2020-06-30 00:00:00"),
        slp_type="H0",
        country="DE",
        state="DE-BY",
        annual_energy_consumption=params["ann_consumption"] / 1000.0,
    )  # input is in MWh

    example_slp_df["time"] = pd.to_datetime(example_slp_df["time"])
    example_slp_df.set_index("time", inplace=True)

    # bin it to an hourly frequency
    df_load = example_slp_df.resample("60T").mean()

    print(f"total load: {df_load['power'].sum()}")

    df = px.data.stocks()
    fig = px.line(df_load, title="Annual consumption of power")
    fig.update_xaxes(dtick="M1", tickformat="%b\n%Y", ticklabelmode="period")
    # fig.show()

    # plt.plot(df_load.index, df_load["power"])
    # plt.show()

    batt_max_cap = []
    pv_n_modules = []
    cost = []
    for n_modules in range(params["n_modules_max"], 0, -1):

        print(f"n_modules: {n_modules}")

        # calculate PV production
        df_pv = pvf.pv_power_forecast(
            params["lat"],
            params["lon"],
            "Liu-Jordan",
            params["pv_module_name"],
            params["inverter_name"],
            params["surface_tilt"],
            params["surface_azimuth"],
            df_weather.index,  #    times
            df_weather["temperature"] - 273.15,  #    temp
            df_weather["wind_speed"],
            df_weather["clouds_all"],
            n_modules,
        )

        print(f"total pv power: {np.sum(df_pv)}")

        # plt.plot(df_pv)
        # plt.show()

        # subtract load from PV --> excess / missing energy
        excess = df_pv - df_load["power"]

        # plt.plot(excess)
        # plt.show()

        # charge / discharge the battery up to an arbitrary capacity
        # assuming an arbitrarily large charging power
        # assuming an initial charge level
        batt_charge = params["ini_charge_level"] * 1000.0 + np.cumsum(excess)

        # if the battery charging state reaches below zero, this is not a feasible solution
        # anymore
        if np.min(batt_charge) < 0.0:
            print(np.min(batt_charge))
            break

        # find largest local maximum, which corresponds to the necessary battery capacity
        # TBD: adjust the window such that the correct min / max is retrieved
        batt_charge_roll = batt_charge.rolling(168).mean().to_numpy()
        ind_local_maxima = argrelextrema(batt_charge_roll, np.greater)
        ind_local_minima = argrelextrema(batt_charge_roll, np.less)

        print("local_maxima: ", batt_charge_roll[ind_local_maxima[0]])

        # needed battery capacity to allow to meet the missing load
        # this is the difference between the highest local maximum and the lowest local minimum
        batt_max_cap.append(
            np.max(batt_charge_roll[ind_local_maxima[0]])
            - np.min(batt_charge_roll[ind_local_minima[0]])
        )
        pv_n_modules.append(n_modules)

        # evaluate cost function
        cost.append(
            params["cost_per_module"] * n_modules
            + params["cost_per_kWh"] * batt_max_cap[-1] / 1000.0
        )

        # plt.plot(batt_charge)
        # plt.show()

    # filter the optimal solution
    print(cost)
    print(pv_n_modules)
    print(batt_max_cap)

    ind_optimal = np.argmin(np.array(cost))

    print("optimal solution")
    print(f"number of modules: {pv_n_modules[ind_optimal]}")
    print(f"battery capacity (watts): {batt_max_cap[ind_optimal]}")

    return pv_n_modules[ind_optimal], int(batt_max_cap[ind_optimal])
